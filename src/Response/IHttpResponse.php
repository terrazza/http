<?php
namespace Terrazza\Component\Http\Response;
use Psr\Http\Message\ResponseInterface;

interface IHttpResponse extends ResponseInterface {}